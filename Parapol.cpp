#include "Parapol.h"
#include<iostream>
using namespace std;
void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}
void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0, y = 0, p;
	// Area 1
	Draw2Points(xc, yc, x, y, ren);
	p = 1 - A;
	while (x <= A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	// Area 2
	p = 2 * A - 1;
	while ((x >= A) && (x<700))
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x=0, y=0, p;
	// Area 1
	Draw2Points(xc, yc, x, y, ren);
	p = 1 - A;
	while (x <= A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	// Area 2
	p = 2 * A - 1;
	while ((x >= A)&&(x<700))
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
}
