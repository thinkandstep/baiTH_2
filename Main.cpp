#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "Line.h"
using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);


	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };
	SDL_SetRenderDrawColor(ren, colorCurve.a, colorCurve.b, colorCurve.g, colorCurve.r);
	DrawCurve3(ren, p1, p2, p3, p4);
	SDL_SetRenderDrawColor(ren, colorRect.a, colorRect.b, colorRect.g, colorRect.r);
	SDL_RenderDrawRect(ren, rect1);
	SDL_RenderDrawRect(ren, rect2);
	SDL_RenderDrawRect(ren, rect3);
	SDL_RenderDrawRect(ren, rect4);
	DDA_Line(p1.x, p1.y, p2.x, p2.y, ren);
	DDA_Line(p2.x, p2.y, p3.x, p3.y, ren);
	DDA_Line(p3.x, p3.y, p4.x, p4.y, ren);
	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	bool inside1 = false;
	bool inside2 = false;
	bool inside3 = false;
	bool inside4 = false;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				int x, y;
				SDL_GetMouseState(&x, &y);
				if ((x > rect1->x) && (y > rect1-> y) && (x < (rect1->x + rect1->w)) && (y < (rect1->y + rect1->h)))
				{
					inside1 = true;
					continue;
				}
				if ((x > rect2->x) && (y > rect2->y) && (x < (rect2->x + rect2->w)) && (y < (rect2->y + rect2->h)))
				{
					inside2 = true;
					continue;
				}
				if ((x > rect3->x) && (y > rect3->y) && (x < (rect3->x + rect3->w)) && (y < (rect3->y + rect3->h)))
				{
					inside3 = true;
					continue;
				}
				if ((x > rect4->x) && (y > rect4->y) && (x < (rect4->x + rect4->w)) && (y < (rect4->y + rect4->h)))
				{
					inside4 = true;
					continue;
				}
			}
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				int x, y;
				SDL_GetMouseState(&x, &y);
				if (inside1)
				{
					inside1 = false;
					p1.x = x;
					p1.y = y;
					rect1->x = p1.x - 3;
					rect1->y = p1.y - 3;
				}
				if (inside2)
				{
					inside2 = false;
					p2.x = x;
					p2.y = y;
					rect2->x = p2.x - 3;
					rect2->y = p2.y - 3;
				}
				if (inside3)
				{
					inside3 = false;
					p3.x = x;
					p3.y = y;
					rect3->x = p3.x - 3;
					rect3->y = p3.y - 3;
				}
				if (inside4)
				{
					inside4 = false;
					p4.x = x;
					p4.y = y;
					rect4->x = p4.x - 3;
					rect4->y = p4.y - 3;
				}

				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);

				SDL_SetRenderDrawColor(ren, colorCurve.a, colorCurve.b, colorCurve.g, colorCurve.r);
				DrawCurve3(ren, p1, p2, p3, p4);
				SDL_SetRenderDrawColor(ren, colorRect.a, colorRect.b, colorRect.g, colorRect.r);
				SDL_RenderDrawRect(ren, rect1);
				SDL_RenderDrawRect(ren, rect2);
				SDL_RenderDrawRect(ren, rect3);
				SDL_RenderDrawRect(ren, rect4);
				DDA_Line(p1.x, p1.y, p2.x, p2.y, ren);
				DDA_Line(p2.x, p2.y, p3.x, p3.y, ren);
				DDA_Line(p3.x, p3.y, p4.x, p4.y, ren);
				SDL_RenderPresent(ren);
				continue;
			}
			if (event.type == SDL_MOUSEMOTION)
			{
				continue;
			}
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
